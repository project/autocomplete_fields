<?php

function _autocomplete_field_config(){

  $table = array(
    '#theme' => 'table',
    '#header' => array(t('Menu'), t('#autocomplete_path'), t('description')),
    '#rows' => array(
        array('Users\'s registered mail id', 'mail/autocomplete', 'This field will generate a autocomplete field populating user\'s mail ids'),
        array('Content title', 'node/title/autocomplete', 'This field will generate a autocomplete field populating node\'s title'),
   ),
  );
  return $table;
}
