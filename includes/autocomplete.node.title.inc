<?php
/**
* autocomplete helper
* $string = string for search
*/
function _node_title_autocomplete($string){
  $matches = array();
  $result = db_select('node', 'n')
    ->fields('n', array('title'))
    ->condition('title', '%' . db_like($string) . '%', 'LIKE')
    ->execute();
  // save the query to matches
  foreach ($result as $row) {
    $matches[$row->title] = check_plain($row->title);
  }
  // Return the result to the form in json
  drupal_json_output($matches);
}