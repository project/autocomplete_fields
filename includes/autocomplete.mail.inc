<?php
/**
* autocomplete helper
* $string = string for search
*/
function _user_mail_autocomplete($string){
  $matches = array();
  $result = db_select('users', 'u')
    ->fields('u', array('mail'))
    ->condition('mail', '%' . db_like($string) . '%', 'LIKE')
    ->execute();
  // save the query to matches
  foreach ($result as $row) {
    $matches[$row->mail] = check_plain($row->mail);
  }
  // Return the result to the form in json
  drupal_json_output($matches);
}